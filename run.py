import cv2
import mediapipe as mp
import numpy as np
from scipy.interpolate import griddata


class Face_UVmap:
    def __init__(self):
        self.face_mesh = mp.solutions.face_mesh.FaceMesh(static_image_mode=True,
                                                         max_num_faces=1,
                                                         refine_landmarks=True,
                                                         min_detection_confidence=0.5)

        self.lm_list = []

        # 3D MODEL
        # self.modelLeftMouth = [628, 360]
        # self.modelRightMouth = [676, 360]
        self.modelLeftMouth = (625, 362)
        self.modelRightMouth = (680, self.modelLeftMouth[1])
        # self.modelMiddleMouth = [self.modelLeftMouth[0] + (self.modelRightMouth[0] - self.modelLeftMouth[0]) // 2,
        #                          self.modelLeftMouth[1] - (self.modelRightMouth[0] - self.modelLeftMouth[0]) // 2]

        # self.modelLeftEye_left = [595, 290]
        # self.modelLeftEye_right = [590, 290]
        # self.modelLeftEye_top = [593, 287]
        # self.modelLeftEye_bottom = [593, 293]

        self.modelLeftEye_left = [595, 290]
        self.modelLeftEye_right = [633, 291]
        self.modelLeftEye_top = [610, 282]
        self.modelLeftEye_bottom = [610, 294]

        self.modelLeftEye = (self.modelLeftEye_left, self.modelLeftEye_right, self.modelLeftEye_top, self.modelLeftEye_bottom)

        self.modelRightEye_left = [720, 290]
        self.modelRightEye_right = [710, 290]

        self.modelMouthWidth = self.modelRightMouth[0] - self.modelLeftMouth[0]
        self.modelEyeWidth = self.modelRightEye_right[0] - self.modelLeftEye_left[0]
        self.modelMouthEyeHeight = self.modelLeftMouth[1] - self.modelLeftEye_left[1]

    def detect_lm(self, image: np.ndarray, crop=None):
        """
        :param image: BGR image
        """
        # Convert the BGR image to RGB before processing.
        if crop is None:
            h, w, _ = image.shape
            results = self.face_mesh.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            self.lm_list = []
            for lm in results.multi_face_landmarks[0].landmark:
                self.lm_list.append([int(lm.x * w), int(lm.y * h)])

            self.c1 = [0, 0]
            self.c13 = [0, h // 2]
            self.c12 = [w // 2, 0]
            self.c2 = [w, 0]
            self.c3 = [0, h]
            self.c4 = [w, h]

            self.corners = np.array((self.c1, self.c2, self.c3, self.c4))

        else:
            x, y, w, h = crop
            results = self.face_mesh.process(cv2.cvtColor(image[y:y + h, x:x + w], cv2.COLOR_BGR2RGB))
            self.lm_list = []
            for lm in results.multi_face_landmarks[0].landmark:
                self.lm_list.append([int(lm.x * w + x), int(lm.y * h + y)])
                #     fg = cv2.circle(fg, p, 1, (0, 255, 0), -1)

            self.c1 = [x, y]
            self.c2 = [x + w, y]
            self.c3 = [x, y + h]
            self.c4 = [x + w, y + h]
            self.corners = np.array((self.c1, self.c2, self.c3, self.c4))

        self.lm_list = np.array(self.lm_list)

        self.leftEye_left = self.lm_list[33]
        self.leftEye_right = self.lm_list[243]
        # self.leftEye_top = self.lm_list[470]
        # self.leftEye_bottom = self.lm_list[145]
        # self.leftEye = np.array(( self.leftEye_left, self.leftEye_right,
        #                           self.leftEye_top, self.leftEye_bottom ))

        #left, right, top, bottom
        self.leftEye = self.lm_list[[33, 243, 470, 145]]

        self.rightEye_left = self.lm_list[362]
        self.rightEye_right = self.lm_list[263]
        # self.rightEye_top = self.lm_list[475]
        # self.rightEye_bottom = self.lm_list[374]
        # self.rightEye = np.array(( self.rightEye_left, self.rightEye_right,
        #                           self.rightEye_top, self.rightEye_bottom ))
        self.rightEye = self.lm_list[[362, 263, 475, 374]]

        # self.nose = self.lm_list[[9, 122, 351, 196, 419, 174, 399, 217, 437]]
        self.nose = self.lm_list[[2, 1, 4, 5, 195, 197, 6, 168, 8, 9,]]
                                  #55,107,285,336]]#brown

        self.leftMouth = self.lm_list[61,]
        self.rightMouth = self.lm_list[291,]
        # self.middleMouth = (self.leftMouth[0] + (self.rightMouth[0] - self.leftMouth[0]) // 2,
        #                     self.leftMouth[1] - (self.rightMouth[0] - self.leftMouth[0]) // 2)

        self.mouthWidth = self.rightMouth[0] - self.leftMouth[0]
        self.eyeWidth = self.rightEye_right[0] - self.leftEye_left[0]
        self.mouthEyeHeight = self.leftMouth[1] - self.leftEye_left[1]

        self.outers = np.squeeze(cv2.convexHull(self.lm_list), axis=1)

        self.leftEyeBrow = self.lm_list[[156,70]]


    def map_face(self, gender: str, image: np.ndarray) -> None:
        """
        :param gender: 'man', 'woman'
        :param image(str/np.ndarray): path to image or npimage
        :return:None
        """

        if isinstance(image, str):
            image = cv2.imread(image)
        self.detect_lm(image)

        if gender == 'man':
            bg = cv2.imread('Men_NoFace.png')
        elif gender == 'woman':
            bg = cv2.imread('Women_noFace.png')



        src = np.concatenate((
            self.outers,
            self.leftEye,
            self.rightEye,
            self.nose,
            # self.leftEyeBrow,
            np.expand_dims(self.leftMouth, axis=0),#-2
            np.expand_dims(self.rightMouth, axis=0),#-1
            # np.expand_dims(self.leftEye_left, axis=0),#-1
            # np.expand_dims(self.leftEye_right, axis=0),#-1
            # np.expand_dims(self.rightEye_left, axis=0),#-1
            # np.expand_dims(self.rightEye_right, axis=0),#-1
            )).astype(np.float32).copy()

        outers_idx = (0,self.outers.shape[0])
        leftEye_idx = (outers_idx[1], outers_idx[1] + self.leftEye.shape[0])
        rightEye_idx = (leftEye_idx[1], leftEye_idx[1] + self.rightEye.shape[0])
        nose_idx = (rightEye_idx[1], rightEye_idx[1] + self.nose.shape[0])
        # leftEyeBrow_idx = (nose_idx[1], nose_idx[1] + self.leftEyeBrow.shape[0])

        #resize
        fx = self.modelEyeWidth / self.eyeWidth
        fy = self.modelMouthEyeHeight / self.mouthEyeHeight
        dst = src.copy() * (fx,fy)

        #move to right pos
        dst += self.modelLeftEye_left - dst[leftEye_idx[0]]

        #map left eye
        # print(dst[leftEye_idx[0]:leftEye_idx[1]])
        dst[leftEye_idx[0]:leftEye_idx[1]] = np.array(self.modelLeftEye, dtype=np.float32)

        #map mouth
        dst[-2] = self.modelLeftMouth
        dst[-1] = self.modelRightMouth



        # dst[36:40, 0] -= dst[37,0] - self.modelLeftEye_right[0]
        # dst[leftEye_idx[0]:leftEye_idx[1]] += (-20,10)
        # dst[rightEye_idx[0]:rightEye_idx[1]] += (20,10)
        # dst[leftEyeBrow_idx[0]:leftEyeBrow_idx[1]] -= (20,0)


        h, w = bg.shape[:2]
        grid_x, grid_y = np.mgrid[0:w - 1:w * 1j, 0:h - 1:h * 1j]
        grid_z = griddata(
            np.flip(dst, axis=1),
            np.flip(src, axis=1),
            (grid_x, grid_y), method='cubic')

        map_x = np.append([], [ar[:, 1] for ar in grid_z]).reshape(w, h).astype('float32')
        map_y = np.append([], [ar[:, 0] for ar in grid_z]).reshape(w, h).astype('float32')
        warped = cv2.remap(image, map_x, map_y, cv2.INTER_CUBIC)

        mask = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
        mask[mask > 0] = 255


        X,Y,width,height = (cv2.boundingRect( dst[outers_idx[0]:outers_idx[1]].astype(np.int) ))

        dst = cv2.seamlessClone(
            warped,
            bg,
            mask,
            (X+width//2, Y+height//2),
            cv2.MIXED_CLONE
        )


        # cv2.imshow('fg',fg)
        # cv2.imshow('bg',bg)
        # cv2.namedWindow('dst_fg', cv2.WINDOW_NORMAL)

        # cv2.imshow('dst',dst)
        # cv2.imshow('mask', mask)
        cv2.imshow('warped', warped)
        cv2.imshow('image', image)
        cv2.waitKey(0)

        cv2.imwrite('output.png', dst)


if __name__ == '__main__':
    mapper = Face_UVmap()
    # mapper.map_face('man', '/home/duc/Desktop/workspace/tekup/data_skin_color/data/medium_skin/medium_skin_32.png')
    # mapper.map_face('man', '/home/duc/Desktop/workspace/tekup/data_skin_color/data/fair_skin/fair_skin_6.png')
    # mapper.map_face('man', '/home/duc/Desktop/workspace/tekup/data_skin_color/data/fair_skin/fair_skin_10.png')
    mapper.map_face('man', '/home/duc/Desktop/workspace/tekup/data_skin_color/data/fair_skin/fair_skin_16.png')
    # mapper.map_face('man', 'trump.jpg')
    # mapper.map_face('man', 'ngo-ngang-voi-ve-dep-cua-hot-girl-anh-the-chua-tron-18-docx-1622043349706.jpeg')
