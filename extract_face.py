import cv2
import mediapipe as mp
import numpy as np
from pprint import pprint

path = '/home/duc/Desktop/workspace/tekup/data_skin_color/data/medium_skin/medium_skin_32.png'
# path = '14vid-Obama-mediumSquareAt3X-v3.jpg'
# path = 'trump.jpg'

face_mesh = mp.solutions.face_mesh.FaceMesh(static_image_mode=True,
                                  max_num_faces=1,
                                  refine_landmarks=True,
                                  min_detection_confidence=0.5)

image = cv2.imread(path)
image = cv2.resize(image, dsize=(None,None), fx=6, fy=6)
# __image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
bgra = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

# Convert the BGR image to RGB before processing.
results = face_mesh.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

h,w = image.shape[:2]
_list = []
for lm in results.multi_face_landmarks[0].landmark:
    _list.append((int(lm.x*w),int(lm.y*h)))

print(len(_list))

pprint([ f'{i}:{e}' for i,e in enumerate(_list) ])

# mattrai, matphai, mepmoitrai, giuamoi, mepmoiphai
# i_list = [133,362,61,14,291]
# i = 112
# i = 160
# i = 291
# i = 61
# i = 31
# i = 255
# while True:
for i,p in enumerate(_list):
# for i in (7,31,32,33):
    image = cv2.circle(image, _list[i], 3, (0,255,0), -1)
    image = cv2.putText(image, str(i), _list[i], 1, 1, (0,255,0), 1)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image', image)
cv2.imwrite('keypts.png', image)
cv2.waitKey(0)
i += 1

# if key == ord('q'):
#     break
# else:

# rows,cols,ch = image.shape
# print(_list[133], _list[362], _list[61], _list[291])
# pts1 = np.float32((_list[133], _list[362], _list[61], _list[291]))
# pts2 = np.float32(((450-10, 346), (541+10, 344), _list[61], _list[291]))
#
# M, _ = cv2.findHomography(pts1, pts2)
# dst = cv2.warpPerspective(image, M,(cols,rows))


# M = cv2.getAffineTransform(pts1,pts2)
# dst = cv2.warpAffine(image,M,(cols,rows))
cv2.imshow('img', image)
cv2.imshow('dst', dst)
cv2.waitKey(0)

# image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
# image = image.astype(np.int)
# image += (50,50,50)
# print(np.max(image))
# image = image.astype(np.uint8)
# print(np.max(image))
# image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

# contours = cv2.convexHull( np.array(_list) )

# mask = np.zeros((h,w))
# mask = cv2.drawContours( mask, [contours], -1, 255, -1)

# kernel = np.ones((6, 6), np.uint8)
# mask = cv2.erode(mask, kernel)

# x,y,w,h = cv2.boundingRect(contours)
# bgra = bgra[y:y+h,x:x+w]
# bgra = cv2.medianBlur(bgra, 9)
# # bgra = cv2.blur(bgra, (11,11))



# mask = mask[y:y+h,x:x+w]
# bgra[:,:,3] = mask

# cv2.imwrite(f'{path.split(".")[0]}.png',
#             bgra)
# cv2.imshow('bgra', bgra[:,:,:3])
# cv2.waitKey(0)